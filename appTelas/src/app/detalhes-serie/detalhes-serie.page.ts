import { Component, OnInit } from '@angular/core';
import { DetalhesSeriePageModule } from './detalhes-serie.module';

@Component({
  selector: 'app-detalhes-serie',
  templateUrl: './detalhes-serie.page.html',
  styleUrls: ['./detalhes-serie.page.scss'],
})
export class DetalhesSeriePage { 
  imagem = [
    {
    capa1: 'https://upload.wikimedia.org/wikipedia/pt/thumb/7/71/Brooklyn_Nine-Nine-5.jpg/230px-Brooklyn_Nine-Nine-5.jpg'
    }
  ];
  constructor() { }

  ngOnInit() {
  }

}
